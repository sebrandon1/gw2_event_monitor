GW2_Event_Monitor
=================

The GW2 event monitor is an application that will be using the GW2 API layer provided by ArenaNet.

The API allows users to access live data within the game and create dynamic content via RESTful calls.

This project is a playground for my brother and I to create an ASP.net based website that will have multiple
pages with different functions.

Rough set of requirements:
* Event "Predictor" page.
  --  This page will try and predict (based on the start/finish times of previous events) the next upcoming event
      and where to go with your character to obtain meta-reward chests.

* Personalized Event Monitor
  --  This page will allow the user to select only the events they would like to watch.
      
