﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Diagnostics;
using System.Threading;

namespace Event_Timer_Updater
{
    class Program
    {
        static void Main(string[] args)
        {
            GW2_Helper gw2 = new GW2_Helper();

            // Grab a list of the worlds to traverse through.
            DataTable WorldsList = gw2.GetWorldNames();

            Console.WriteLine("GW2 Event History Program - Running since: " + DateTime.Now.ToString());
            Console.WriteLine("-------------------------------------------------------------");

            // Loop through all available events updating the current timer.
            while (true)
            {
                int eventsInserted = 0;
                int eventsDeleted = 0;

                // Grab the current build.
                int currentBuild = gw2.GetBuildID();

                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();

                // Grab the events that we want watched.
                DataTable WatchedEvents = GetWatchedEventsList();

                // For each of the worlds, grab the status of the Watched Events.
                foreach (DataRow worldRow in WorldsList.Rows)
                {
                    string world_id = worldRow["id"].ToString();

                    Stopwatch worldWatch = new Stopwatch();
                    worldWatch.Start();

                    Console.Write("Current World ID: " + world_id);

                    // Retrieve all of the events from the server.
                    DataTable currentEvents = gw2.EventsList(world_id, "", "");

                    // Grab the status of the event in the given world.
                    foreach (DataRow watchedEventRow in WatchedEvents.Rows)
                    {
                        string watchedEventID = watchedEventRow["EVENT_ID"].ToString();

                        // Traverse the current events and find the given event_id.
                        foreach (DataRow currentEventRow in currentEvents.Rows)
                        {
                            string currentRowEventID = currentEventRow["event_id"].ToString();
                            string currentEventRowState = currentEventRow["state"].ToString();

                            // Match the event id's for the given world.
                            if (currentRowEventID == watchedEventID)
                            {
                                // Check if the status has changed from the previous time.
                                InsertNewEventState(currentRowEventID, world_id, currentEventRowState, ref eventsInserted, currentBuild);
                                break;
                            } // if

                        } // foreach

                    } // foreach

                    worldWatch.Stop();
                    TimeSpan tsWorld = worldWatch.Elapsed;

                    string elapsedWorldTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                                                    tsWorld.Hours, tsWorld.Minutes, tsWorld.Seconds,
                                                    tsWorld.Milliseconds / 10);

                    Console.WriteLine(" Elapsed Time: " + elapsedWorldTime);

                } // foreach

                // Delete OLD Event Information
                DeleteOldEvents(ref eventsDeleted);
                DeleteOldBuildEvents(ref eventsDeleted);

                // Stop the 
                stopWatch.Stop();
                TimeSpan ts = stopWatch.Elapsed;

                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                                                    ts.Hours, ts.Minutes, ts.Seconds,
                                                    ts.Milliseconds / 10);

                Console.WriteLine("Number of Events Inserted: " + eventsInserted);
                Console.WriteLine("Number of Events Deleted: " + eventsDeleted);
                Console.WriteLine("Time elasped this round: " + elapsedTime);
                Console.WriteLine("-------------------------------------------------------------");

            } // INFINITE LOOP
        }

        static DataTable GetWatchedEventsList()
        {
            // Variable Declaration
            DataTable WatchedEventsTable;

            // Create Query for grabbing the watched events.
            string sString1 = "SELECT EVENT_ID FROM [GW2_Database].[dbo].[EVENTS_TO_WATCH]";

            WatchedEventsTable = ReturnTable(sString1);

            // Return the DataTable with the watched events.
            return WatchedEventsTable;
        }

        static void DeleteOldBuildEvents(ref int eventsDeleted)
        {
            // Object Declaration
            GW2_Helper gw2 = new GW2_Helper();

            // Grab the current build number.
            int currentBuild = gw2.GetBuildID();

            // Create a query.
            string dQuery = "DELETE FROM EVENT_HISTORY WHERE BUILD != '" + currentBuild.ToString() +"'";

            // Run the query.
            if (RunEmptyQuery(dQuery))
            {
                eventsDeleted++;
            }
        }

        static void DeleteOldEvents(ref int eventsDeleted)
        {
            string dString = "DELETE FROM EVENT_HISTORY "
                           + "WHERE DATE_RECORDED < '" + DateTime.Now.AddDays(-2).ToShortDateString() + "'";

            // Delete any events that have been recorded 100 times each.
            if (RunEmptyQuery(dString))
            {
                eventsDeleted++;
            }
        }

        static void InsertNewEventState(string event_id, string world_id, string currentState, ref int eventsInserted, int currentBuild)
        {
            // Queries.
            string countQuery = "SELECT COUNT(*) FROM EVENT_HISTORY "
                                + "WHERE 1=1 "
                                + "AND RTRIM(EVENT_ID) = '" + event_id.Trim() + "' "
                                + "AND RTRIM(SERVER_ID) = '" + world_id.Trim() + "' "
                                + "";

            string lastStateQuery = "SELECT RTRIM(STATE) AS STATE FROM EVENT_HISTORY "
                                  + "WHERE 1=1 "
                                  + "AND RTRIM(EVENT_ID) = '" + event_id.Trim() + "' "
                                  + "AND RTRIM(SERVER_ID) = '" + world_id.Trim() + "' "
                                  + "ORDER BY DATE_RECORDED DESC, TIME_RECORDED DESC ";

            string iQuery = "INSERT INTO EVENT_HISTORY "
                          + "(SERVER_ID, EVENT_ID, STATE, DATE_RECORDED, TIME_RECORDED, BUILD) "
                          + "VALUES ('"
                          + world_id.Trim() + "', '"
                          + event_id.Trim() + "', '"
                          + currentState.Trim() + "', '"
                          + DateTime.Now.ToShortDateString()
                          + "', '" + DateTime.Now.ToShortTimeString() + "',"
                          + "'" + currentBuild.ToString() + "')";

            // If the event is already in the database.
            if (ReturnSQLString(countQuery) != "0")
            {
                // Grab the last known recorded state.
                string lastState = ReturnSQLString(lastStateQuery);

                // If the last state recorded doesn't match the current state, insert a new state.
                if (lastState != currentState)
                {
                    // Run the insert query.
                    if (RunEmptyQuery(iQuery))
                    {
                        eventsInserted++;
                    }

                } // if

            } // if
            else
            {
                // Store the NEW event to the table.
                if (RunEmptyQuery(iQuery))
                {
                    eventsInserted++;
                }
            }
        }

        static string GetConnString()
        {
            return "Server=brandonserver.ath.cx,49020;Database=GW2_Database;User Id=Brandon;Password=palm;";
        }

        static string GetMySQLConnString()
        {
            return "Server=localhost;Database=GW2;Uid=Brandon;Pwd=palm1234;";
        }

        static DataTable ReturnTable(string query)
        {
            DataTable resultTable = new DataTable();

            using (SqlConnection conn1 = new SqlConnection(GetConnString()))
            {
                SqlCommand cmd = new SqlCommand(query, conn1);
                SqlDataReader reader = null;

                try
                {
                    conn1.Open();
                    reader = cmd.ExecuteReader();
                    resultTable.Load(reader);
                }
                catch
                {
                    resultTable = null;
                }
                finally
                {
                    if (reader != null)
                    {
                        reader.Dispose();
                    }
                    
                    cmd.Dispose();
                    conn1.Close();
                }
            }

            return resultTable;
        }

        static bool RunEmptyQuery(string query)
        {
            using (SqlConnection sqlConn = new SqlConnection(GetConnString()))
            {
                SqlCommand cmd = new SqlCommand(query, sqlConn);

                try
                {
                    sqlConn.Open();
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        static string ReturnSQLString(string query)
        {
            string result = "";

            using (SqlConnection sqlConn = new SqlConnection(GetConnString()))
            {
                // Create a new SQL Command.
                SqlCommand cmd = new SqlCommand(query, sqlConn);
                SqlDataReader reader = null;

                try
                {
                    sqlConn.Open(); // Open connection.
                    reader = cmd.ExecuteReader();
                    reader.Read();

                    // Grab the result.
                    result = reader[0].ToString();
                }
                catch
                {
                    result = "ERROR";
                }
                finally
                {
                    if (reader != null)
                    {
                        reader.Dispose();
                    }
                    
                }
            }

            return result;
        }

    }
}
