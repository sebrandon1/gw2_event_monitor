﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Xml;
using RestSharp;
using Newtonsoft.Json;
using System.Net;
using Codeplex.Data;

namespace Event_Timer_Updater
{
    public class GW2_Helper
    {
		// Base URL for all of the URL calls.  Useful when they move from v1 to v2.
		static string baseURL = "https://api.guildwars2.com/v1/";
	
        // API Calls via RESTful calls.
        string eventNames_URL       = baseURL + "event_names.json";
        string mapNames_URL         = baseURL + "map_names.json";
        string worldNames_URL       = baseURL + "world_names.json";
		string items_URL            = baseURL + "items.json";
        string itemDetails_URL      = baseURL + "item_details.json";            // Required parameter: item_id
        string recipes_URL          = baseURL + "recipes.json"; 
        string recipeDetails_URL    = baseURL + "recipe_details.json";          // Required parameter: recipe_id
        string guildDetails_URL     = baseURL + "guild_details.json";           // Required parameter: guild_id or guild_name
        string buildID_URL          = baseURL + "build.json";
        string dyes_URL             = baseURL + "colors.json";
        string allEventsURL         = baseURL + "events.json";    
		
		// WVW API Calls
        string wvwMatches_URL       = baseURL + "wvw/matches.json";             // Returns a list of match_id's.
        string matchDetails_URL     = baseURL + "wvw/match_details.json";       // Required parameter: match_id
        string objectiveNames_URL   = baseURL + "wvw/objective_names.json";

        // DONE
        public DataTable GetWorldNames()
        {
            // Create a DataTable to hold all of the names in the world.
            DataTable WorldNamesTable = new DataTable("WorldNamesTable");
            WorldNamesTable.Columns.Add("id", typeof(string));
            WorldNamesTable.Columns.Add("name", typeof(string));           
            
            // Create a WebClient and download the JSON string from GW2.
            WebClient webClient = new WebClient();
            var objectJson = DynamicJson.Parse(webClient.DownloadString(worldNames_URL));
            var array1 = objectJson.Deserialize<WorldNames[]>();

            // Traverse through the array and store into the DataTable.
            foreach (WorldNames item in array1)
            {
                WorldNamesTable.Rows.Add(item.id, item.name); // WORLD ID, WORLD NAME
            }

            // Return the Datatable full of world names.
            return WorldNamesTable;
        }

        // DONE
        public DataTable GetWorldNames(int regionNum)
        {
            /*
             * This function accepts a 1 or a 2 for the NA or Europe server choices.
             * */

            // DataTable declaration for holding the world names.
            DataTable RegionWorldNames = new DataTable();
            RegionWorldNames.Columns.Add("id", typeof(string));
            RegionWorldNames.Columns.Add("name", typeof(string)); 

            // Create new WebClient to call the API.
            WebClient webClient = new WebClient();
            var objectJson = DynamicJson.Parse(webClient.DownloadString(worldNames_URL));
            var array1 = objectJson.Deserialize<WorldNames[]>();

            if (regionNum == 1)
            {
                foreach (WorldNames item in array1)
                {
                    if (Convert.ToString(item.id[0]) == "1")
                    {
                        RegionWorldNames.Rows.Add(item.id, item.name);
                    }
                }

            } // if
            else if (regionNum == 2)
            {
                foreach (WorldNames item in array1)
                {
                    if (Convert.ToString(item.id[0]) == "2")
                    {
                        RegionWorldNames.Rows.Add(item.id, item.name);
                    }
                }

            } // else if
            else
            {
                // Incorrect region specified.
                RegionWorldNames = null;
            } // else

            // Return the table full of WorldNames.
            return RegionWorldNames;
        }

        // DONE
        public DataTable GetMapNames()
        {
            // Table to hold all of the map names.
            DataTable MapNamesTable = new DataTable();
            MapNamesTable.Columns.Add("id", typeof(string));
            MapNamesTable.Columns.Add("name", typeof(string));

            // Create a WebClient and download the string from GW2.
            WebClient webClient = new WebClient();
            var mapJsonArray = DynamicJson.Parse(webClient.DownloadString(mapNames_URL));
            var mapArray = mapJsonArray.Deserialize<MapNames[]>();

            // Traverse through the array and store into the DataTable.
            foreach(MapNames item in mapArray)
            {
                MapNamesTable.Rows.Add(item.id, item.name);
            }
            
            // Return the Datatable full of map names.
            return MapNamesTable;
        }

        // NOT DONE
        public DataTable GetWvWMatches()
        {
            // Create a new DataTable to store the returned matches and values.
            DataTable wvwMatchesTable = new DataTable();
            wvwMatchesTable.Columns.Add("MATCHLIST");

            // Create a WebClient and download the array from GW2.
            WebClient webClient = new WebClient();
            var matchesJsonArray = DynamicJson.Parse(webClient.DownloadString(wvwMatches_URL));
            var matchesArray = matchesJsonArray.Deserialize<WvwMatchesList[]>();

            foreach (WvwMatchesList match in matchesArray)
            {
                //List<WvwMatch> matchList = match.wvw_matches;
            }

            // Return the table full of match information.
            return wvwMatchesTable;
        }

        // DONE
        public DataTable GetEventNames()
        {
            // Create new event name table holder.
            DataTable EventNameTable = new DataTable();
            EventNameTable.Columns.Add("id", typeof(string));
            EventNameTable.Columns.Add("name", typeof(string));

            // Create a WebClient and download the string from GW2.
            WebClient webClient = new WebClient();
            var eventJsonArray = DynamicJson.Parse(webClient.DownloadString(eventNames_URL));
            var eventArray = eventJsonArray.Deserialize<EventNames[]>();

            // Traverse the array and store to table.
            foreach (EventNames item in eventArray)
            {
                EventNameTable.Rows.Add(item.id, item.name);
            }

            // Return the event names in the DataTable.
            return EventNameTable;
        }

        // DONE
        public int GetBuildID()
        {
            // Create a WebClient and download the string from GW2.
            WebClient webClient = new WebClient();
            var buildJsonArray = DynamicJson.Parse(webClient.DownloadString(buildID_URL));

            // Store the ID into the BuildID.
            BuildID id = buildJsonArray.Deserialize<BuildID>();

            // Return the build ID.
            return id.build_id;
        }

        // NOT DONE
        public DataTable GetMatchDetails(string match_id)
        {
            // Create a new table for storing the Match Details.
            DataTable MatchDetailsTable = new DataTable();
            MatchDetailsTable.Columns.Add("MATCH_ID", typeof(string));
            MatchDetailsTable.Columns.Add("SCORES", typeof(DataTable));
            MatchDetailsTable.Columns.Add("MAPS", typeof(DataTable));

            // Append the id of the match to the end of the URL.
            string tempURL = matchDetails_URL;
            tempURL += "?match_id=" + match_id.Trim();

            // Create a WebClient to download the array from GW2.
            WebClient webClient = new WebClient();
            var matchDetailsJsonArray = DynamicJson.Parse(webClient.DownloadString(tempURL));
            var detailsArray = matchDetailsJsonArray.Deserialize<WvwDetailsList[]>();

            // MATCH_ID, SCORES, MAPS
            foreach (WvwDetailsList item in detailsArray)
            {
                DataTable scoresTable = new DataTable();
                scoresTable.Columns.Add("SCORES", typeof(int));

                foreach(var scoreItem in item.scores)
                {
                    scoresTable.Rows.Add(Convert.ToInt32(scoreItem));
                }

                // Create a new DataTable to hold maps info.
                DataTable mapsTable = new DataTable();
                mapsTable.Columns.Add("type", typeof(string));
                mapsTable.Columns.Add("scores", typeof(int));
                mapsTable.Columns.Add("objective", typeof(DataTable));

                foreach (var mapItem in item.maps)
                {
                    // Create new DataTable to hold objectives info.
                    DataTable objectivesTable = new DataTable();
                    objectivesTable.Columns.Add("ID", typeof(int));
                    objectivesTable.Columns.Add("OWNER", typeof(string));
                    objectivesTable.Columns.Add("OWNER_GUILD", typeof(string));

                    foreach (var objItem in mapItem.objectives)
                    {
                        // Store the objective id, owner, and owner guild to the table.
                        objectivesTable.Rows.Add(objItem.id, objItem.owner, objItem.owner_guild);
                    }

                    // Store the type, scores, and all of the objectives into the maps table.
                    mapsTable.Rows.Add(mapItem.type, mapItem.scores, objectivesTable);
                }

                // Store the match id, all of the overall scores, and all of the maps and corresponding scores into the table.
                MatchDetailsTable.Rows.Add(item.match_id, scoresTable, mapsTable);
            }

            // Return the table full of match details.
            return MatchDetailsTable;
        }

        // DONE
        public DataTable GetWvwObjectiveNames()
        {
            // Create a new table for WvW Objective Names.
            DataTable ObjectiveNamesTable = new DataTable();
            ObjectiveNamesTable.Columns.Add("ID", typeof(string));
            ObjectiveNamesTable.Columns.Add("NAME", typeof(string));

            // Create a webclient to call the REST API.
            WebClient webClient = new WebClient();
            var wvwObjectiveArray = DynamicJson.Parse(webClient.DownloadString(objectiveNames_URL));
            var objectiveArray = wvwObjectiveArray.Deserialize<ObjectiveNames[]>();

            // Traverse the JSON and store to table.
            foreach (ObjectiveNames item in objectiveArray)
            {
                ObjectiveNamesTable.Rows.Add(item.id, item.name);
            }

            // Return the objective names in a table.
            return ObjectiveNamesTable;
        }

        // DONE
        public DataTable EventsList(string world_id, string map_id, string event_id)
        {
            #region Function Notes
            /*
             * Completed 6-28-2013 by Brandon.
             * */
            #endregion

            // Booleans for parameters.
            bool worldFlag = false;
            bool mapFlag = false;

            // Store the URL into a temp variable.
            string tempURL = allEventsURL;

            // If parameters were passed in - add the ? into the URL.
            if (world_id != "" || map_id != "" || event_id != "")
                tempURL += "?";

            // If World ID was specified.
            if (world_id != "")
            {
                tempURL += "world_id=" + world_id;
                worldFlag = true;
            }

            // If Map ID was specified.
            if (map_id != "")
            {
                if (worldFlag)
                {
                    tempURL += "&map_id=" + map_id;
                }
                else
                {
                    tempURL += "map_id=" + map_id;
                    mapFlag = true;
                }
            }

            // If Event ID was specified.
            if (event_id != "")
            {
                if (worldFlag || mapFlag)
                {
                    tempURL += "&event_id=" + event_id;
                }
                else
                {
                    tempURL += "event_id=" + event_id;
                }
            }
            
            // Create a DataTable to store all of the events.
            DataTable eventsTable = new DataTable();
            eventsTable.Columns.Add("world_id", typeof(string));
            eventsTable.Columns.Add("map_id", typeof(string));
            eventsTable.Columns.Add("event_id", typeof(string));
            eventsTable.Columns.Add("state", typeof(string));
            
            // Create a WebClient and download the string from GW2.
            WebClient webClient = new WebClient();
            var eventJsonArray = webClient.DownloadString(new Uri(tempURL));
            var j = JsonConvert.DeserializeObject <AllEvents>(eventJsonArray);

            int ctr = 0;
            while (ctr < j.events.Count)
            {
                eventsTable.Rows.Add(j.events[ctr].world_id, j.events[ctr].map_id, j.events[ctr].event_id, j.events[ctr].state);
                ctr++;
            }

            return eventsTable;
        }
        
        // DONE
        public string GetSpecificEventStatus(string world_id, string event_id)
        {
            #region Function Notes
            /*
             * Completed 6-28-2013 by Brandon.
             * */
            #endregion

            // Variable Declaration
            string status = "";

            // Grab specific event information for a given server.
            DataTable EventTable = EventsList(world_id, "", event_id);

            // Grab the first row.
            if (EventTable != null && EventTable.Rows[0] != null)
            {
                // Grab the status.
                status = EventTable.Rows[0]["state"].ToString();
            }
            else
            {
                status = "";
            }

            return status;
        }

        // DONE
        public string GetSpecificWorldName(string ID)
        {
            #region Function Notes
            /*
             * Added 6-19-2013 by Lucas.
             * */
            #endregion

            // Variable Declarations
            string worldName = "";
            DataTable worldNamesTable = new DataTable();

            worldNamesTable = GetWorldNames();

            foreach (DataRow row in worldNamesTable.Rows)
            {
                if (row["ID"].ToString() == ID)
                {
                    worldName = row["NAME"].ToString();
                    break;
                }
            }

            return worldName;
        }

        // DONE
        public string GetSpecificEventName(string ID)
        {
            #region Function Notes
            /*
             * Added 6-19-2013 by Lucas.
             * */
            #endregion

            // Variable Declarations
            string eventName = "";
            DataTable eventNamesTable = new DataTable();

            eventNamesTable = GetEventNames();

            foreach (DataRow row in eventNamesTable.Rows)
            {
                if (row["ID"].ToString() == ID)
                {
                    eventName = row["NAME"].ToString();
                    break;
                }
            }

            return eventName;
        }

        // DONE
        public string GetSpecificMapName(string ID)
        {
            #region Function Notes
            /*
             * Added 6-19-2013 by Lucas.
             * */
            #endregion

            // Variable Declarations
            string mapName = "";
            DataTable mapNamesTable = new DataTable();

            mapNamesTable = GetMapNames();

            foreach (DataRow row in mapNamesTable.Rows)
            {
                if (row["ID"].ToString() == ID)
                {
                    mapName = row["NAME"].ToString();
                    break;
                }
            }

            return mapName;
        }

        // DONE
        public string GetSpecificObjectiveName(string ID)
        {
            #region Function Notes
            /*
             * Added 6-19-2013 by Lucas.
             * */
            #endregion

            // Variable Declarations
            string objectiveName = "";
            DataTable objectiveNamesTable = new DataTable();

            objectiveNamesTable = GetWvwObjectiveNames();

            foreach (DataRow row in objectiveNamesTable.Rows)
            {
                if (row["ID"].ToString() == ID)
                {
                    objectiveName = row["NAME"].ToString();
                    break;
                }
            }

            return objectiveName;
        }
    }

    #region JSON Public Classes
    /* 
     * Note: These are the getter and setter classes for the GW2 API JSON calls.
     *       Used so the JSON parser knows what "datatype" the arrays are.
     * */

    public class WorldNames
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class MapNames
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class EventNames
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class BuildID
    {
        public int build_id { get; set; }
    }

    public class WvwMatch // Part of WvwMatchesList
    {
        public string wvw_match_id { get; set; }
        public int red_world_id { get; set; }
        public int blue_world_id { get; set; }
        public int green_world_id { get; set; }
        public string start_time { get; set; }
        public string end_time { get; set; }
    }

    public class WvwMatchesList
    {
        public List<WvwMatch> wvw_matches { get; set; }
    }

    public class Objective // Part of WvwDetailsList
    {
        public int id { get; set; }
        public string owner { get; set; }
        public string owner_guild { get; set; }
    }

    public class Map // Part of WvwDetailsList
    {
        public string type { get; set; }
        public List<int> scores { get; set; }
        public List<Objective> objectives { get; set; }
    }

    public class WvwDetailsList // 3 Classes Overall
    {
        public string match_id { get; set; }
        public List<int> scores { get; set; }
        public List<Map> maps { get; set; }
    }

    public class ObjectiveNames
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Event
    {
        public int world_id { get; set; }
        public int map_id { get; set; }
        public string event_id { get; set; }
        public string state { get; set; }
    }

    public class AllEvents
    {
        public List<Event> events { get; set; }
    }
    
    #endregion

}