﻿&lt;%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs"
Inherits="GW2_Event_Monitor.Default" %&gt;
<title>GW2 Analytics</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap-responsive.css">
<link rel="stylesheet" href="https://djyhxgczejc94.cloudfront.net/builds/4101de2b9d215b115979d8658fe70d7af99dea9b.css">
<div class="background">
  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container">
        <a class="brand" href="#">GW2 Dev Sandbox</a>
        <div class="navbar-content">
          <ul class="nav ">
            <li class="active">
              <a href="#">Home</a> 
            </li>
            <li>
              <a href="#">About</a> 
            </li>
            <li>
              <a href="Event_Predictor.aspx">Predictor</a> 
            </li>
            <li>
              <a href="Subscriber.aspx">Subscriber</a> 
            </li>
            <li>
              <a href="Helper.aspx">Helper</a> 
            </li>
            <li>
              <a href="Watcher.aspx">Watcher</a> 
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="hero-unit hidden-phone">
      <h1>The GW2 Dev Sandbox</h1>
      <p>Choose an application, and enjoy!</p>
      <p></p>
    </div>
    <a class="btn btn-large btn-primary btn-block visible-phone" href="#"><span class="btn-label">Sign Up Today!</span></a>
    <div class="row main-features">
      <div class="span4">
        <div class="well">
          <h3>Meta Event Predictor</h3>
          <p>This application will recommend the next available meta event for your
            characters!</p>
          <a class="btn btn-primary" href="#"><span class="btn-label">Find Out More</span></a> 
        </div>
        <div class="well">
          <h3>Customized Event Watcher</h3>
          <p>An event watcher, customized for you! &nbsp;Choose any and all events
            YOU would like to watch.</p>
          <a class="btn btn-primary">Find Out More<br></a>
          <a></a> 
        </div>
      </div>
      <div class="span4">
        <div class="well">
          <h3>Commander Helper</h3>
          <p>The #1 tool for Commanders in WvW.</p>
          <a class="btn btn-primary" href="#"><span class="btn-label">Find Out More</span></a> 
        </div>
      </div>
      <div class="span4">
        <div class="well">
          <h3>Event Subscriber</h3>
          <p>Subscribe to any and all events based on your server.</p>
          <a class="btn btn-info"
          href="#"><span class="btn-label">Find Out More</span></a> 
        </div>
      </div>
      <div class="span4">
        <div class="well">
          <h3>World vs. World Watcher</h3>
          <p>Watch a match up and dominate your opponents in real time!</p>
          <a class="btn btn-success" href="#"><span class="btn-label">Find Out More</span></a> 
        </div>
      </div>
    </div>
  </div>
</div>