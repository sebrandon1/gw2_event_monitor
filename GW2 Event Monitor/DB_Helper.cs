﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/*
 * Class Notes:
 * 
 *      9-31-2013:  Created class to house all of the database-centric functions.
 *                  Functions will be generic for use later.  GW2 specific queries will be
 *                  declared in the GW2_Helper.cs class with an interface to this class. (B. Palm)
 * 
 * */

namespace GW2_Event_Monitor
{
    public class DB_Helper
    {
        bool RunEmptyQuery(string query)
        {
            // Variables
            bool result = false;

            return result;
        }

        string ReturnFirstString(string query)
        {
            // Variables
            string result = "";

            return result;
        }

        DataTable ReturnTable(string query)
        {
            // Variables
            DataTable resultTable = new DataTable();

            return resultTable;
        }


    }
}