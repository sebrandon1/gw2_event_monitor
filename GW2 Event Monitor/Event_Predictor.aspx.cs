﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml;
using System.Globalization;
using Newtonsoft.Json;
using System.IO;

namespace GW2_Event_Monitor
{
    public partial class Event_Predictor : System.Web.UI.Page
    {
        // Class Object Declarations
        GW2_Helper gw2 = new GW2_Helper();
        DB_Helper dbh = new DB_Helper();

        protected void Page_Load(object sender, EventArgs e)
        {
            // Create a new control, then grab whichever control caused the Post Back.
            Control ctrl = new Control();
            ctrl = GetPostBackControl(this.Page);
            
            if (!IsPostBack)    // First Page Load
            {
                RegionChoiceDDL.DataBind();
                PopulateServerDropDown();

                // If the server choice has already been selected. (7-5-2013)
                if (Session["ServerChoice"] != null)
                {
                    ServerChoiceDDL.SelectedValue = (string)Session["ServerChoice"];
                }
            }
            else if (ctrl != null && ctrl.ID != "ServerChoiceDDL") // If the control that caused the postback wasn't the server choice.
            {
                PopulateServerDropDown();
            }

            // Get upcoming event statuses.
            GetCurrentEventStatuses();

            // Store the selected value to the session.
            Session["ServerChoice"] = ServerChoiceDDL.SelectedValue;
        }

        protected void GetCurrentEventStatuses()
        {
            // Grab all of the events for the given server choice.
            DataTable ServerEvents = gw2.EventsList((string)ServerChoiceDDL.SelectedValue, "", "");

            // Dragon Events.
            GetJormagStatus(ServerEvents);
            GetShattererStatus(ServerEvents);
            GetTequatlStatus(ServerEvents);

            // Other Events.
            GetMawStatus(ServerEvents);
            GetBehemothStatus(ServerEvents);
            GetFireElementalStatus(ServerEvents);
            GetJungleWurmStatus(ServerEvents);
            GetGolemStatus(ServerEvents);
            GetMegaDestroyerStatus(ServerEvents);

            return;
        }

        protected string GetTimeLeft(string event_id, string server_id)
        {
            // Variable Declarations
            string timeLeft = "";

            // Create a query for the event ID from the SQL Server Database.
            string sQuery = "SELECT DATE_RECORDED, TIME_RECORDED "
                          + "FROM GW2_Database.Event_History "
                          + "WHERE EVENT_ID = '" + event_id + "' AND SERVER_ID = '" + server_id + "'";

            // Run the query and return the date and time recorded.


            // Return the time left on the event.
            return timeLeft;
        }

        protected void GetMawStatus(DataTable eventsList)
        {
            #region Event Information
            // http://wiki.guildwars2.com/wiki/The_Frozen_Maw
			
			// 1. Protect Tor the Tall's supplies from the grawl.
			// 2. Protect Scholar Brogun as he investigates the grawl tribe.
			// 3. Destroy the dragon totem.
			// 4. Defeat the shaman's elite guard.
			// 5. Defeat the Svanir shamans spreading the dragon's corruption.
			// 6. Destroy the corrupted portals summoning creatures from the mists.
            // 7. Kill the Svanir shaman chief to break his control over the ice elemental.
            #endregion

            // Event ID's
			string event1 = "6F516B2C-BD87-41A9-9197-A209538BB9DF";
			string event2 = "D5F31E0B-E0E3-42E3-87EC-337B3037F437";
			string event3 = "6565EFD4-6E37-4C26-A3EA-F47B368C866D";
			string event4 = "90B241F5-9E59-46E8-B608-2507F8810E00";
			string event5 = "DB83ABB7-E5FE-4ACB-8916-9876B87D300D";
			string event6 = "374FC8CB-7AB7-4381-AC71-14BFB30D3019";
			string event7 = "F7D9D427-5E54-4F12-977A-9809B23FBA99";

            // Variable Declarations
            string serverChoice = (string)ServerChoiceDDL.SelectedValue;
            string status = "";
            System.Drawing.Color myColor;

            // Get the TimeLeft according to the SQL database.
            string timeLeft = GetTimeLeft(event7, serverChoice);

            // Grab the events' statuses.
            string event1Status = gw2.GetSpecificEventStatus(serverChoice, event1, eventsList);
            string event2Status = gw2.GetSpecificEventStatus(serverChoice, event2, eventsList);
            string event3Status = gw2.GetSpecificEventStatus(serverChoice, event3, eventsList);
            string event4Status = gw2.GetSpecificEventStatus(serverChoice, event4, eventsList);
            string event5Status = gw2.GetSpecificEventStatus(serverChoice, event5, eventsList);
            string event6Status = gw2.GetSpecificEventStatus(serverChoice, event6, eventsList);
            string event7Status = gw2.GetSpecificEventStatus(serverChoice, event7, eventsList);

            // Evaluate the statuses.
            if (event7Status == "Active" || event7Status == "Preparation")
            {
                status = "Kill the Shaman!";
                myColor = System.Drawing.Color.LimeGreen;
            }
            else if (event1Status == "Warmup" && event2Status == "Success" && event3Status == "Success"
                && event4Status == "Success" && event5Status == "Success" && event6Status == "Success")
            {
                // This is the primary status situation that means we are in between events.
                status = "In Between Events!";
                myColor = System.Drawing.Color.Empty;
            }

            // PRE-EVENT STATUSES
            else if (event1Status == "Active" || event1Status == "Preparation")
            {
                status = "P1: Protect Supplies!";
                myColor = System.Drawing.Color.LimeGreen;
            }
            else if (event2Status == "Active" || event2Status == "Preparation")
            {
                status = "P2: Protect Brogun!";
                myColor = System.Drawing.Color.LimeGreen;
            }
            else if (event3Status == "Active" || event3Status == "Preparation")
            {
                status = "P3: Destroy Totem!";
                myColor = System.Drawing.Color.LimeGreen;
            }
            else if (event4Status == "Active" || event4Status == "Preparation")
            {
                status = "P4: Defeat Guard!";
                myColor = System.Drawing.Color.LimeGreen;
            }
            else if (event5Status == "Active" || event5Status == "Preparation")
            {
                status = "P5: Defeat Shamans!";
                myColor = System.Drawing.Color.LimeGreen;
            }
            else if (event6Status == "Active" || event6Status == "Preparation")
            {
                status = "P6: Destroy Portals!";
                myColor = System.Drawing.Color.LimeGreen;
            }

            // MAIN-EVENT FAILURE OR SUCCESS
            else if (event7Status == "Fail")
            {
                status = "Failed!";
                myColor = System.Drawing.Color.Red;
            }
            else if (event7Status == "Success")
            {
                status = "Completed!";
                myColor = System.Drawing.Color.Empty;
            }
            else
            {
                status = "Pre Event";
                myColor = System.Drawing.Color.LimeGreen;
            }

            // Generate new table.
            GenerateEventTable("The Frozen Maw", status, myColor, timeLeft, ref TimeSlotTBL_4);

            // Write to file.
            WriteStatusToFile(event7Status, serverChoice, event7 );
			
        }

        protected void GetBehemothStatus(DataTable eventsList)
        {
            #region Function Notes
            /*  7-22-2013 -- Hopefully successfully completed this function.
             *               My Logic is this:
             *                  If the event is active or in preparation, it is "ACTIVE".
             *                  If the 1st event is in warmup and the rest are a success, then we are "WAITING" between events.
             *                  If the main event is in fail mode, it is "FAILED".
             *                  If none of these conditions, it is in Pre Event mode. 
             * */
            #endregion

            #region Event Information
            // http://wiki.guildwars2.com/wiki/Secrets_in_the_Swamp

            // 1. Drive back Underworld creatures by destroying portals in the Heartwoods.
            // 2. Drive back Underworld creatures by destroying portals in the monastery.
            // 3. Drive back Underworld creatures by destroying portals in Taminn Foothills.
            // 4. Drive back Underworld creatures by destroying portals in the swamp.
            // 5. Defeat the shadow behemoth.
            #endregion

            // Event ID's
			string event1 = "31CEBA08-E44D-472F-81B0-7143D73797F5";
            string event2 = "E539A5E3-A33B-4D5F-AEED-197D2716F79B";
            string event3 = "AFCF031A-F71D-4CEA-85E1-957179414B25";
            string event4 = "36330140-7A61-4708-99EB-010B10420E39";
            string event5 = "31CEBA08-E44D-472F-81B0-7143D73797F5";

            // Variable Declarations
            string serverChoice = (string)ServerChoiceDDL.SelectedValue;
            string status = "";
            System.Drawing.Color myColor = System.Drawing.Color.Empty;


            // Get the TimeLeft according to the SQL database.
            string timeLeft = GetTimeLeft(event5, serverChoice);

            // Grab the events' statuses.
            string event1Status = gw2.GetSpecificEventStatus(serverChoice, event1, eventsList);
            string event2Status = gw2.GetSpecificEventStatus(serverChoice, event2, eventsList);
            string event3Status = gw2.GetSpecificEventStatus(serverChoice, event3, eventsList);
            string event4Status = gw2.GetSpecificEventStatus(serverChoice, event4, eventsList);
            string event5Status = gw2.GetSpecificEventStatus(serverChoice, event5, eventsList);

            // Evaluate the statuses.
            if (event5Status == "Active" || event5Status == "Preparation")
            {
                status = "Kill Behemoth!";
                myColor = System.Drawing.Color.LimeGreen;
            }
            else if (event1Status == "Warmup" && event2Status == "Success" && event3Status == "Success" && event4Status == "Success")
            {
                status = "Completed!";
                myColor = System.Drawing.Color.Empty;
            }

            // PRE-EVENT DETAILS
            else if (event1Status == "Active" || event1Status == "Preparation")
            {
                status = "P1: Heartwoods Portals!";
                myColor = System.Drawing.Color.LimeGreen;
            }
            else if (event2Status == "Active" || event2Status == "Preparation")
            {
                status = "P2: Monastery Portals!";
                myColor = System.Drawing.Color.LimeGreen;
            }
            else if (event3Status == "Active" || event3Status == "Preparation")
            {
                status = "P3: Taminn Foothills Portals!";
                myColor = System.Drawing.Color.LimeGreen;
            }
            else if (event4Status == "Active" || event4Status == "Preparation")
            {
                status = "P4: Swamp Portals!";
                myColor = System.Drawing.Color.LimeGreen;
            }

            // MAIN EVENT FAILURE OR SUCCESS
            else if (event5Status == "Fail")
            {
                status = "Failed";
                myColor = System.Drawing.Color.Red;
            }
            else
            {
                status = "Pre Event";
                myColor = System.Drawing.Color.LimeGreen;
            }

            // Generate new table.
            GenerateEventTable("Shadow Behemoth", status, myColor, timeLeft, ref TimeSlotTBL_5);
        }

        protected void GetFireElementalStatus(DataTable eventsList)
        {
            #region Event Information
            // http://wiki.guildwars2.com/wiki/Thaumanova_Reactor_Fallout 

            // 1. Destroy the chaotic materials created by the reactor meltdown.
            // 2. Escort the C.L.E.A.N. 5000 golem while it absorbs clouds of chaos magic.
            // 3. Defend the C.L.E.A.N. 5000 golem.
            // 4. Destroy the fire elemental created from chaotic energy fusing with the C.L.E.A.N. 5000's energy core.
            #endregion

            // Event ID's
            string event1 = "6B897FF9-4BA8-4EBD-9CEC-7DCFDA5361D8";
            string event2 = "5E4E9CD9-DD7C-49DB-8392-C99E1EF4E7DF";
            string event3 = "2C833C11-5CD5-4D96-A4CE-A74C04C9A278";
            string event4 = "33F76E9E-0BB6-46D0-A3A9-BE4CDFC4A3A4";

            // Variable Declarations
            string serverChoice = (string)ServerChoiceDDL.SelectedValue;
            string status = "";
            System.Drawing.Color myColor = System.Drawing.Color.Empty;


            // Get the TimeLeft according to the SQL database.
            string timeLeft = GetTimeLeft(event4, serverChoice);

            // Grab the events' statuses.
            string event1Status = gw2.GetSpecificEventStatus(serverChoice, event1, eventsList);
            string event2Status = gw2.GetSpecificEventStatus(serverChoice, event2, eventsList);
            string event3Status = gw2.GetSpecificEventStatus(serverChoice, event3, eventsList);
            string event4Status = gw2.GetSpecificEventStatus(serverChoice, event4, eventsList);

            // Evaluate the statuses.
            if (event4Status == "Active" || event4Status == "Preparation")
            {
                status = "Active";
                myColor = System.Drawing.Color.LimeGreen;
            }
            else if (event1Status == "Warmup" || event2Status == "Warmup" || event3Status == "Warmup" || event4Status == "Warmup")
            {
                status = "Waiting";
                myColor = System.Drawing.Color.Empty;
            }

            // PRE-EVENT LOGIC
            else if (event1Status == "Active" || event1Status == "Preparation")
            {
                status = "P1: Destroy Materials!";
                myColor = System.Drawing.Color.LimeGreen;
            }
            else if (event2Status == "Active" || event2Status == "Preparation")
            {
                status = "P2: Escort Golem!";
                myColor = System.Drawing.Color.LimeGreen;
            }
            else if (event3Status == "Active" || event3Status == "Preparation")
            {
                status = "P3: Defend Golem!";
                myColor = System.Drawing.Color.LimeGreen;
            }
            
            // MAIN EVENT FAILURE OR SUCCESS
            else if (event4Status == "Fail")
            {
                status = "Failed";
                myColor = System.Drawing.Color.Red;
            }
            else if (event4Status == "Success")
            {
                status = "Completed";
                myColor = System.Drawing.Color.Empty;
            }
            else
            {
                status = "Pre Event";
                myColor = System.Drawing.Color.LimeGreen;
            }

            // Add table.
            GenerateEventTable("Fire Elemental", status, myColor, timeLeft, ref TimeSlotTBL_6);
        }

        protected void GetJungleWurmStatus(DataTable eventsList)
        {
            #region Event Information
            // http://wiki.guildwars2.com/wiki/The_Battle_for_Wychmire_Swamp

            // 1. Protect Gamarien as he scouts Wychmire Swamp.
            // 2. Destroy the blighted growth.
            // 3. Kill the giant blighted grub.
            // 4. Destroy the avatars of blight.
            // 5. Defeat the great jungle wurm.
            #endregion

            string event1 = "613A7660-8F3A-4897-8FAC-8747C12E42F8";
            string event2 = "CF6F0BB2-BD6C-4210-9216-F0A9810AA2BD";
            string event3 = "1DCFE4AA-A2BD-44AC-8655-BBD508C505D1";
            string event4 = "61BA7299-6213-4569-948B-864100F35E16";
            string event5 = "C5972F64-B894-45B4-BC31-2DEEA6B7C033";

            // Variable Declarations
            string serverChoice = (string)ServerChoiceDDL.SelectedValue;
            string status = "";
            System.Drawing.Color myColor = System.Drawing.Color.Empty;

            // Get the TimeLeft according to the SQL database.
            string timeLeft = GetTimeLeft(event5, serverChoice);

            // Grab the events' statuses.
            string event1Status = gw2.GetSpecificEventStatus(serverChoice, event1, eventsList);
            string event2Status = gw2.GetSpecificEventStatus(serverChoice, event2, eventsList);
            string event3Status = gw2.GetSpecificEventStatus(serverChoice, event3, eventsList);
            string event4Status = gw2.GetSpecificEventStatus(serverChoice, event4, eventsList);
            string event5Status = gw2.GetSpecificEventStatus(serverChoice, event5, eventsList);

            // Evaluate the statuses.
            if (event5Status == "Active" || event5Status == "Preparation")
            {
                status = "Active";
                myColor = System.Drawing.Color.LimeGreen;
            }
            if (event1Status == "Warmup" || event2Status == "Warmup" || event3Status == "Warmup" || event4Status == "Warmup")
            {
                status = "Waiting";
                myColor = System.Drawing.Color.Empty;
            }
            else if (event5Status == "Fail")
            {
                status = "Failed";
                myColor = System.Drawing.Color.Red;
            }
            else if (event5Status == "Success")
            {
                status = "Completed";
                myColor = System.Drawing.Color.Empty;
            }
            else
            {
                status = "Pre Event";
                myColor = System.Drawing.Color.LimeGreen;
            }

            // Add the rows to the table.
            GenerateEventTable("Jungle Wurm", status, myColor, timeLeft, ref TimeSlotTBL_7);
        }

        protected void GetGolemStatus(DataTable eventsList)
        {
            // http://wiki.guildwars2.com/wiki/Disable_the_containers_before_they_release_their_toxins

            // 1. Disable the containers before they release their toxins.
            // 2. Defeat the Inquest's golem Mark II.

            string event1 = "3ED4FEB4-A976-4597-94E8-8BFD9053522F";
            string event2 = "9AA133DC-F630-4A0E-BB5D-EE34A2B306C2";

            // Variable Declarations
            string serverChoice = (string)ServerChoiceDDL.SelectedValue;
            string status = "";
            System.Drawing.Color myColor = System.Drawing.Color.Empty;

            // Get the TimeLeft according to the SQL database.
            string timeLeft = GetTimeLeft(event2, serverChoice);

            // Grab the events' statuses.
            string event1Status = gw2.GetSpecificEventStatus(serverChoice, event1, eventsList);
            string event2Status = gw2.GetSpecificEventStatus(serverChoice, event2, eventsList);

            // Evaluate the statuses.
            if (event2Status == "Active" || event2Status == "Preparation")
            {
                status = "Active";
                myColor = System.Drawing.Color.LimeGreen;
            }
            else if (event1Status == "Warmup" && (event2Status == "Success" || event2Status == "Fail"))
            {
                // Note: The Golem event has two different paths based on whether or not the pre-event
                //       is successfully completed or not.
                status = "Waiting";
                myColor = System.Drawing.Color.Empty;
            }
            else if (event1Status == "Fail" || event2Status == "Fail")
            {
                status = "Failed";
                myColor = System.Drawing.Color.Red;
            }
            else if (event2Status == "Success")
            {
                status = "Completed";
                myColor = System.Drawing.Color.Empty;
            }
            else
            {
                status = "Pre Event";
                myColor = System.Drawing.Color.LimeGreen;
            }

            // Add the rows to the table.
            GenerateEventTable("Mark II Golem", status, myColor, timeLeft, ref TimeSlotTBL_8);
        }

        protected void GetMegaDestroyerStatus(DataTable eventsList)
        {
            #region Event Information
            // http://wiki.guildwars2.com/wiki/The_Battle_for_Mount_Maelstrom

            // 1. Stop the Inquest's destroyer-essence collectors by destroying their power cores.
            // 2. Defend the Advanced Arcanomics lab.
            // 3. Stop the rampaging Inquest golem.
            // 4. Slay destroyers, and close their fissures on the volcano's north side.
            // 5. Eliminate destroyer forces in the volcano's southeast cavern.
            // 6. Eliminate destroyer forces on the volcano's south side.
            // 7. Protect the asura and their technology while they quell the unstable volcano.
            #endregion

            string event1 = "294E08F6-CA36-42B3-8D06-B321BA06EECA";
            string event2 = "61D4579A-C53F-4C26-A31B-92FABE3DA566";
            string event3 = "3BA29A69-A30B-405D-96AC-CBA5D511C163";
            string event4 = "9E5D9F1A-FE14-49C6-917F-43AAE227165C";
            string event5 = "584A4D22-33DC-4D77-A5D9-2FA7379401ED";
            string event6 = "4210CE81-BDB7-448E-BE33-46E18A5A3477";
            string event7 = "36E81760-7D92-458E-AA22-7CDE94112B8F";
            string event8 = "C876757A-EF3E-4FBE-A484-07FF790D9B05";

            // Variable Declarations
            string serverChoice = (string)ServerChoiceDDL.SelectedValue;
            string status = "";
            System.Drawing.Color myColor = System.Drawing.Color.Empty;

            // Get the TimeLeft according to the SQL database.
            string timeLeft = GetTimeLeft(event8, serverChoice);

            // Grab the events' statuses.
            string event1Status = gw2.GetSpecificEventStatus(serverChoice, event1, eventsList);
            string event2Status = gw2.GetSpecificEventStatus(serverChoice, event2, eventsList);
            string event3Status = gw2.GetSpecificEventStatus(serverChoice, event3, eventsList);
            string event4Status = gw2.GetSpecificEventStatus(serverChoice, event4, eventsList);
            string event5Status = gw2.GetSpecificEventStatus(serverChoice, event5, eventsList);
            string event6Status = gw2.GetSpecificEventStatus(serverChoice, event6, eventsList);
            string event7Status = gw2.GetSpecificEventStatus(serverChoice, event7, eventsList);
            string event8Status = gw2.GetSpecificEventStatus(serverChoice, event8, eventsList);

            // Evaluate the statuses.
            if (event8Status == "Active" || event8Status == "Preparation")
            {
                status = "Active";
                myColor = System.Drawing.Color.LimeGreen;
            }
            else if (event1Status == "Warmup" || event2Status == "Warmup" || event3Status == "Warmup" || event4Status == "Warmup" || event5Status == "Warmup" || event6Status == "Warmup" || event7Status == "Warmup")
            {
                status = "Waiting";
                myColor = System.Drawing.Color.Empty;
            }
            else if (event8Status == "Fail")
            {
                status = "Failed";
                myColor = System.Drawing.Color.Red;
            }
            else if (event8Status == "Success")
            {
                status = "Completed";
                myColor = System.Drawing.Color.Empty;
            }

            // Add the rows to the table.
            GenerateEventTable("Mega Destroyer", status, myColor, timeLeft, ref TimeSlotTBL_9);
        }

        protected void GetShattererStatus(DataTable eventsList)
        {
            #region Event Information
            // http://wiki.guildwars2.com/wiki/Slay_the_Shatterer

            // 1. Collect siege weapon pieces for Crusader Blackhorn
            // 2. Escort the Sentinel squad to the Vigil camp in Lowland Burns.
            // 3. Slay the Shatterer
            #endregion

            #region Event ID's
            string event1 = "580A44EE-BAED-429A-B8BE-907A18E36189";
            string event2 = "8E064416-64B5-4749-B9E2-31971AB41783";
            string event3 = "03BF176A-D59F-49CA-A311-39FC6F533F2F";
            #endregion

            // Variable Declarations
            string serverChoice = (string)ServerChoiceDDL.SelectedValue;
            string status = "";
            System.Drawing.Color myColor = System.Drawing.Color.Empty;

            // Get the TimeLeft according to the SQL database.
            string timeLeft = GetTimeLeft(event3, serverChoice);

            // Grab the events' statuses.
            string event1Status = gw2.GetSpecificEventStatus(serverChoice, event1, eventsList);
            string event2Status = gw2.GetSpecificEventStatus(serverChoice, event2, eventsList);
            string event3Status = gw2.GetSpecificEventStatus(serverChoice, event3, eventsList);

            // Evaluate the statuses.
            if (event3Status == "Active" || event3Status == "Preparation")
            {
                status = "Active";
                myColor = System.Drawing.Color.LimeGreen;
            }
            else if (event1Status == "Warmup" || event2Status == "Warmup")
            {
                status = "Waiting";
                myColor = System.Drawing.Color.Empty;
            }
            else if (event3Status == "Fail")
            {
                status = "Failed";
                myColor = System.Drawing.Color.Red;
            }
            else if (event3Status == "Success")
            {
                status = "Completed";
                myColor = System.Drawing.Color.Empty;
            }

            // Add the rows to the table.
            GenerateEventTable("The Shatterer", status, myColor, timeLeft, ref TimeSlotTBL_2);

            return;
        }

        protected void GetTequatlStatus(DataTable eventsList)
        {
            #region Event Information
            // http://wiki.guildwars2.com/wiki/Danger_at_Fabled_Djannor

            // 1. Defend Chokevine Gorge before Risen overrun it.
            // 2. Defend the supply of energy from krait.
            // 3. Escort the quaggans to Quaztocel.
            // 4. Defeat Tequatl the Sunless.
            #endregion

            // Event Strings
            string event1 = "C9F3EAA5-ADEF-4F52-B798-0914EA96C8E6";
            string event2 = "350DB073-033E-4609-A007-98A83F9976E0";
            string event3 = "4B212997-CEF0-4B2C-91BE-B787A6A32DE9";
            string event4 = "568A30CF-8512-462F-9D67-647D69BEFAED";

            // Grab the server id.
            string serverChoice = (string)ServerChoiceDDL.SelectedValue;
            string status = "";
            System.Drawing.Color myColor = System.Drawing.Color.Empty;

            // Get the TimeLeft according to the SQL database.
            string timeLeft = GetTimeLeft(event4, serverChoice);

            // Grab the event statuses.
            string e1Status = gw2.GetSpecificEventStatus(serverChoice, event1, eventsList);
            string e2Status = gw2.GetSpecificEventStatus(serverChoice, event2, eventsList);
            string e3Status = gw2.GetSpecificEventStatus(serverChoice, event3, eventsList);
            string e4Status = gw2.GetSpecificEventStatus(serverChoice, event4, eventsList);

            // Evaluate the statuses.
            if (e4Status == "Active" || e4Status == "Preparation")
            {
                status = "Active";
                myColor = System.Drawing.Color.LimeGreen;
            }
            else if (e1Status == "Active" || e2Status == "Active" || e3Status == "Active" ||
                    e1Status == "Preparation" || e2Status == "Preparation" || e3Status == "Preparation")
            {
                // Determine SPECIFIC event status.
                if (e1Status == "Active" || e1Status == "Preparation")
                {
                    status = "P1: Kill Risen!";     // Pre-Event 1
                }
                else if (e2Status == "Active" || e2Status == "Preparation")
                {
                    status = "P2: Defend Supply!";  // Pre-Event 2
                }
                else if (e3Status == "Active" || e3Status == "Preparation" && (e1Status == "Success" || e1Status == "Warmup"))
                {
                    status = "P3: Escort Quags!";   // Pre-Event 3
                }
                else if ((e3Status == "Active" || e3Status == "Preparation") && e1Status == "Fail")
                {
                    status = "Waiting!";            // Event is in the long "waiting" state between activities.
                }
                else
                {
                    status = "Unknown!";
                }

                myColor = System.Drawing.Color.LimeGreen;
            }
            else if (e1Status == "Warmup" || e2Status == "Warmup" || e3Status == "Warmup")
            {
                status = "Waiting!";
                myColor = System.Drawing.Color.Empty;
            }
            else if (e4Status == "Fail")
            {
                status = "Failed";
                myColor = System.Drawing.Color.Red;
            }
            else if (e4Status == "Success")
            {
                status = "Completed";
                myColor = System.Drawing.Color.Empty;
            }

            // Add the rows to the table.
            GenerateEventTable("Tequatl", status, myColor, timeLeft, ref TimeSlotTBL_3);

            return;
        }

        protected void GetJormagStatus(DataTable eventsList)
        {
            #region Event Information
            // http://wiki.guildwars2.com/wiki/Breaking_the_Claw_of_Jormag

            // 1. Destroy the dragon crystal on the road to Slough of Despond.          (PRE-EVENT)
            // 2. Destroy the dragon crystal at Elder's Vale.                           (PRE-EVENT)
            // 3. Destroy the dragon crystal near the Pact siege wall.                  (PRE-EVENT)
            // 4. Destroy the dragon crystal at the Pact flak cannons.                  (PRE-EVENT)
            // 5. Lure out the Claws of Jormag by destroying the final dragon crystal.  (PRE-EVENT)
            // 6. Defeat the Claw of Jormag.
            #endregion

            // Event ID's
            string event1 = "0CA3A7E3-5F66-4651-B0CB-C45D3F0CAD95";
            string event2 = "96D736C4-D2C6-4392-982F-AC6B8EF3B1C8";
            string event3 = "C957AD99-25E1-4DB0-9938-F54D9F23587B";
            string event4 = "429D6F3E-079C-4DE0-8F9D-8F75A222DB36";
            string event5 = "BFD87D5B-6419-4637-AFC5-35357932AD2C";
            string event6 = "0464CB9E-1848-4AAA-BA31-4779A959DD71";

            // Variable Declarations
            string serverChoice = (string)ServerChoiceDDL.SelectedValue;
            string status = "";
            System.Drawing.Color myColor = System.Drawing.Color.Empty;

            // Get the TimeLeft according to the SQL database.
            string timeLeft = GetTimeLeft(event6, serverChoice);

            // Grab the status of each event.
            string event1Status = gw2.GetSpecificEventStatus(serverChoice, event1, eventsList);
            string event2Status = gw2.GetSpecificEventStatus(serverChoice, event2, eventsList);
            string event3Status = gw2.GetSpecificEventStatus(serverChoice, event3, eventsList);
            string event4Status = gw2.GetSpecificEventStatus(serverChoice, event4, eventsList);
            string event5Status = gw2.GetSpecificEventStatus(serverChoice, event5, eventsList);
            string event6Status = gw2.GetSpecificEventStatus(serverChoice, event6, eventsList);

            // Check if any of the pre-events are in "Warmup" state.
            if (event1Status == "Active" ||
                event2Status == "Active" ||
                event3Status == "Active" ||
                event4Status == "Active" ||
                event5Status == "Active")
            {
                status = "Pre Event";
                myColor = System.Drawing.Color.LimeGreen;
            }
            else if (event1Status == "Warmup" ||
                event2Status == "Warmup" ||
                event3Status == "Warmup" ||
                event4Status == "Warmup" ||
                event5Status == "Warmup")
            {
                status = "Waiting";
            }
            else if (event6Status == "Preparation" || event6Status == "Active")
            {
                status = "Active";
            }
            else if (event6Status == "Success")
            {
                status = "Complete";
            }
            else if (event6Status == "Inactive")
            {
                status = "Inactive";
            }
            else if (event6Status == "Fail")
            {
                status = "Failed";
            }

            // Store the table to the time slot.
            GenerateEventTable("Jormag", status, myColor, timeLeft, ref TimeSlotTBL_1);

            return;
        }

        protected void WriteStatusToFile(string status, string server_id, string event_id)
        {
            // Write to the stream.
            using (StreamWriter sw = new StreamWriter("C:/GW2/" + server_id + ".txt", true))
            {
                sw.WriteLine("Event: " + event_id + " Status: " + status + " DateTime: " + DateTime.Now.ToString());
            }
        }

        protected string ReadLastStatusFromFile(string event_id, string server_id)
        {
            string path = "C:/GW2/" + server_id + ".txt";
            string lastStatus = "NONE";

            if (File.Exists(path))
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    int i = 0;
                    while (sr.Peek() >= 0)
                    {
                        string line = sr.ReadLine();
                        string status = "";

                        while (line[i] != ':')
                        {

                            i++;
                        }

                        i++;                        

                        int eventCtr = 0;

                        while (line[i] != ' ')
                        {

                            eventCtr++;
                            i++;
                        }

                        

                    } // while
                }
            }

        }

        protected void CalculateEventTimers()
        {
            // Variable Declarations
            int numEvents = 9;

            // TODO: Need to flesh this out to perform some sort of sorting algorithm on the timers.

            return;
        }

        protected void PopulateServerDropDown()
        {
            // Grab all of the names.
            ServerChoiceDDL.DataSource = gw2.GetWorldNames(Convert.ToInt32(RegionChoiceDDL.SelectedValue));

            // Set the values.
            ServerChoiceDDL.DataTextField = "name";
            ServerChoiceDDL.DataValueField = "id";

            // Bind the datasource.
            ServerChoiceDDL.DataBind();
            return;
        }

        protected void AddStringToCell(ref TableCell cell, string desiredString)
        {
            cell.Text = desiredString;           // Store the string to the label.
        }

        public static Control GetPostBackControl(Page page)
        {
            Control control = null;

            string ctrlname = page.Request.Params.Get("__EVENTTARGET");
            if (ctrlname != null && ctrlname != string.Empty)
            {
                control = page.FindControl(ctrlname);
            }
            else
            {
                foreach (string ctl in page.Request.Form)
                {
                    Control c = page.FindControl(ctl);
                    if (c is System.Web.UI.WebControls.Button)
                    {
                        control = c;
                        break;
                    }
                }
            }
            return control;
        }

        protected void GenerateEventTable(string eventName, string status, System.Drawing.Color color, string timeLeft, ref Table tempTable)
        {
            // Create new table rows.
            TableRow tRow1 = new TableRow();
            TableCell NameCell = new TableCell();
            TableCell StatusCell = new TableCell();

            // Add Name and Status to the table row.
            AddStringToCell(ref NameCell, eventName);
            AddStringToCell(ref StatusCell, status);

            TableRow tRow2 = new TableRow();
            TableCell TimeCell = new TableCell();
            TableCell TimerCell = new TableCell();

            // Add time strings to the table row.
            AddStringToCell(ref TimeCell, "Time Until Event:");
            AddStringToCell(ref TimerCell, timeLeft);

            // NameCell Properties
            NameCell.Font.Bold = true;
            NameCell.Font.Size = 14;
            NameCell.Width = 200;
            NameCell.CssClass = "custyFonts";   // Added 8-14-2013 (Brandon)

            // StatusCell Properties
            StatusCell.Font.Size = 12;
            StatusCell.Width = 150;
            StatusCell.HorizontalAlign = HorizontalAlign.Right;
            StatusCell.CssClass = "custyFonts"; // Added 8-14-2013 (Brandon)

            // TimeCell Properties
            TimeCell.Font.Bold = true;
            TimeCell.Font.Size = 12;
            TimeCell.Width = 175;

            // TimerCell Properties
            TimerCell.Font.Size = 12;
            TimerCell.Width = 150;
            TimerCell.HorizontalAlign = HorizontalAlign.Right;

            // Add the cells to the rows.
            tRow1.Cells.Add(NameCell);
            tRow1.Cells.Add(StatusCell);
            tRow2.Cells.Add(TimeCell);
            tRow2.Cells.Add(TimerCell);

            // Add the rows to the table.
            tempTable.Rows.Add(tRow1);
            tempTable.Rows.Add(tRow2);
        }
    }
}