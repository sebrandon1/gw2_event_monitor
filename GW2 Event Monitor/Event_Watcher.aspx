﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Event_Watcher.aspx.cs" Inherits="GW2_Event_Monitor.Event_Watcher" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<!doctype html>
<html>

<head>
    <title>Event Watcher</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
    <link rel="stylesheet" href="https://app.divshot.com/css/bootstrap.css">
    <link rel="stylesheet" href="https://app.divshot.com/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="https://djyhxgczejc94.cloudfront.net/builds/8b38e521aefa439b3d72952ad6e706ff498e96ea.css">
    <script src="https://app.divshot.com/js/jquery.min.js"></script>
    <script src="https://app.divshot.com/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="Scripts/jquery-2.0.2.js"></script>

    <%-- jQuery Functions --%>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#Button1").click(function () {
                $("#ChooseWorldWell").slideDown("slow");
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm._doPostBack('UpdatePanel2', '');

            });

            $("#Button2").click(function () {
                $("#ChooseMapWell").slideDown("slow");
            });

            $("#Button3").click(function () {
                $("#GridViewWell").slideDown();
            });
        });

    </script>

    <%-- CUSTOM STYLING --%>
    <style type="text/css">
        .background {
            background-color: #202020;
        }
    </style>
</head>

<body>
    <form runat="server" id="Form1">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="brand" href="#">GW2 DEVELOPER SANDBOX</a>
                    <div class="navbar-content">
                        <ul class="nav">
                            <li>
                                <a href="Home.aspx">Home</a>
                            </li>
                            <li>
                                <a href="Event_Predictor.aspx">Event Predictor</a>
                            </li>
                            <li class="active">
                                <a href="Event_Watcher.aspx">Event Watcher</a>
                            </li>
                        </ul>
                        <ul class="nav  pull-right">
                            <li>
                                <a href="#">Sign Up</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="hero-unit">
                <h1>Personalized Event</h1>
                <h1>Watcher</h1>
                <p>Monitor only the events of your choosing!</p>
            </div>
            <a class="btn btn-large btn-primary btn-block visible-phone" href="#"><span class="btn-label">Sign Up Today!</span></a>
            <div class="row main-features">
                <div class="span4">
                    <div class="well">
                        <h3>Choose Region</h3>
                        <asp:DropDownList ID="RegionDDL" runat="server">
                            <asp:ListItem Value="1">North America</asp:ListItem>
                            <asp:ListItem Value="2">Europe</asp:ListItem>
                        </asp:DropDownList>
                        <input id="Button1" type="button" value="Submit" class="btn btn-primary" runat="server" />
                    </div>
                </div>
                <div class="span4">
                    <div class="well" id="ChooseWorldWell" style="display: none">
                        <h3>Choose World</h3>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" OnLoad="UpdatePanel2_Load">
                            <ContentTemplate>
                                <asp:DropDownList ID="WorldDDL" runat="server"></asp:DropDownList>
                                <input id="Button2" type="button" value="Submit" class="btn btn-primary" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <a></a>
                    </div>
                </div>
                <div class="span4">
                    <div class="well" id="ChooseMapWell" style="display: none">
                        <h3>Choose Map</h3>
                        <asp:DropDownList ID="MapDDL" runat="server"></asp:DropDownList>
                        <asp:Button ID="ViewEventsBTN" runat="server" Text="View Events" CssClass="btn btn-primary" />
                    </div>
                </div>
            </div>
            <div></div>
            <div></div>
            <div></div>
            <div class="row main-features">
                <div class="span8">
                    <asp:Panel ID="Panel1" runat="server" Visible="false">
                        <div align="center" id="GridViewWell" class="well" visible="false" height="100" overflow="auto">
                            <h3>Event Listing</h3>
                            <asp:Panel ID="Panel2" runat="server">
                                <table style="width: 100%;">
                                    <tr>
                                        <td>
                                            <asp:Button ID="Button4" runat="server" Text="Add Events" CssClass="btn btn-primary" /></td>
                                        <td>
                                            <asp:Button ID="Button5" runat="server" Text="Remove Events" CssClass="btn btn-primary" /></td>
                                        <td>
                                            <asp:Button ID="Button6" runat="server" Text="Button" CssClass="btn btn-primary" /></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:Timer ID="Timer1" runat="server" Interval="5000" OnTick="Timer1_Tick"></asp:Timer>
                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridView1_RowDataBound1" GridLines="None" CellPadding="10">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="myCheckBox" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="event_id" HeaderText="Event Name" />
                                            <asp:BoundField DataField="state" HeaderText="Event Status" />
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:Button ID="Button3" runat="server" Text="Monitor!" CssClass="btn btn-primary" />
                        </div>
                    </asp:Panel>

                </div>
            </div>
        </div>
    </form>
</body>

</html>



