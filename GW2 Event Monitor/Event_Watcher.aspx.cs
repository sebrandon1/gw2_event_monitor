﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml;
using System.Globalization;
using Newtonsoft.Json;

namespace GW2_Event_Monitor
{
    public partial class Event_Watcher : System.Web.UI.Page
    {
        // Delcarae a GW2_Helper object
        static GW2_Helper gw2 = new GW2_Helper();

        // Create a global datatable to hold the list of all events
        static DataTable eventsTable = gw2.GetEventNames();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Session["ChosenEventsTable"] = null;
            }

            // Create a new control, then grab whichever control caused the Post Back.
            Control ctrl = new Control();
            ctrl = GetPostBackControl(this.Page);
            if (ctrl != null && ctrl.ID == "Button3")
            {
                MonitorBTN();
                return;
            }
            else if (ctrl != null && ctrl.ID == "ViewEventsBTN")
            {
                PopulateGridView();
                Session["ChosenEventsTable"] = null;
                Panel1.Visible = true;
                return;
            }

            PopulateWorldList();
            PopulateMapList();
        }

        public static Control GetPostBackControl(Page page)
        {
            Control control = null;

            string ctrlname = page.Request.Params.Get("__EVENTTARGET");
            if (ctrlname != null && ctrlname != string.Empty)
            {
                control = page.FindControl(ctrlname);
            }
            else
            {
                foreach (string ctl in page.Request.Form)
                {
                    Control c = page.FindControl(ctl);
                    if (c is System.Web.UI.WebControls.Button)
                    {
                        control = c;
                        break;
                    }
                }
            }
            return control;
        }

        protected void PopulateWorldList()
        {
            // Set the World Drop down list datsource to the correct datatable
            WorldDDL.DataSource = gw2.GetWorldNames(Convert.ToInt32(RegionDDL.SelectedValue));

            // Set the values.
            WorldDDL.DataTextField = "name";
            WorldDDL.DataValueField = "id";

            // Bind the datasource.
            WorldDDL.DataBind();
        }

        protected void PopulateMapList()
        {
            // Set the World Drop down list datsource to the correct datatable
            MapDDL.DataSource = gw2.GetMapNames();

            // Set the values.
            MapDDL.DataTextField = "name";
            MapDDL.DataValueField = "id";

            // Bind the datasource.
            MapDDL.DataBind();
        }

        protected void PopulateGridView()
        {
            // Set the World Drop down list datsource to the correct datatable
            GridView1.DataSource = gw2.EventsList(WorldDDL.SelectedValue, MapDDL.SelectedValue, "");

            // Bind the datasource.
            GridView1.DataBind();
        }

        protected void MonitorBTN()
        {
            DataTable ChosenEventsTable = new DataTable();
            ChosenEventsTable.Columns.Add("event_id");
            ChosenEventsTable.Columns.Add("state");

            foreach (GridViewRow row in GridView1.Rows)
            {
                if (((CheckBox)row.FindControl("myCheckBox")).Checked)
                {
                    ChosenEventsTable.Rows.Add(gw2.GetSpecificEventID(row.Cells[1].Text, eventsTable), row.Cells[2].Text);
                }
            }

            Session["ChosenEventsTable"] = ChosenEventsTable;

            // Unbind GridView1 from its current datasource and replace it with the new datasource
            GridView1.DataSource = null;
            GridView1.DataSource = ChosenEventsTable;
            GridView1.DataBind();
        }

        protected void GridView1_RowDataBound1(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.Cells[1].Text == "Event Name")
            {
                // Skip the header row
            }
            else
            {
                e.Row.Cells[1].Text = gw2.GetSpecificEventName(e.Row.Cells[1].Text, eventsTable);

                if (e.Row.Cells[2].Text == "Active")
                {
                    e.Row.ForeColor = System.Drawing.Color.LimeGreen;
                }
            }

        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            if (Session["ChosenEventsTable"] != null)
            {
                DataTable ChosenEventsTable = (DataTable)Session["ChosenEventsTable"];

                // Loop through the chosenEventsTable and get the updated statuses of all events
                foreach (DataRow row in ChosenEventsTable.Rows)
                {
                    row["state"] = gw2.GetSpecificEventStatus(WorldDDL.Text, row["event_id"].ToString());
                }

                // Unbind GridView1 from its current datasource and replace it with the new datasource
                GridView1.DataSource = null;
                GridView1.DataSource = ChosenEventsTable;
                GridView1.DataBind();
            }
        }

        protected void UpdatePanel2_Load(object sender, EventArgs e)
        {
            // This is needed in order for the jquery/javascript to cause the update to the panel
        }
    }

}