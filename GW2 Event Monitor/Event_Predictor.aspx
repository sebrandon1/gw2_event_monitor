﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Event_Predictor.aspx.cs" Inherits="GW2_Event_Monitor.Event_Predictor" %>

<!doctype html>
<html>
<head>
    <title>Event Predictor</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
    <%--<link rel="stylesheet" href="https://app.divshot.com/css/bootstrap.css"> --%>
    <%--<link rel="stylesheet" href="https://app.divshot.com/css/bootstrap-responsive.css"> --%>
    <%--<link rel="stylesheet" href="https://djyhxgczejc94.cloudfront.net/builds/4101de2b9d215b115979d8658fe70d7af99dea9b.css"> --%>
    <%--<script src="https://app.divshot.com/js/jquery.min.js"></script> --%>
    <%--<script src="https://app.divshot.com/js/bootstrap.min.js"></script> --%>

    <%-- INCLUDED STYLES AND JQUERY--%>
    <link href="css/bootstrap-responsive.css" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" />
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Cabin+Condensed:400,700' rel='stylesheet' type='text/css'>

    <%-- CUSTOM STYLING --%>
    <style type="text/css">
        .main-features {
            margin-top: 10px;
        }

        .background {
            background-color: #202020;
        }

        /* Added 8-14-2013 by Brandon */      
        .custyFonts {
            font-family: 'Cabin Condensed', sans-serif;
        }

        /* Added 8-14-2013 by Brandon */    
        .custyFontsBold {
            font-family: 'Cabin Condensed', sans-serif;
            font-weight: 700;
        }

        .html5Center {
            padding-left: 0;
            padding-right: 0;
            margin-left: auto;
            margin-right: auto;
            display: block;
        }
    </style>
</head>

<body>
    <form runat="server" id="Form1">
        <div class="background">
            <div class="navbar navbar-fixed-top">
                <div class="navbar-inner">
                    <div class="container">
                        <a class="brand custyFontsBold" href="#">GW2 Dev Sandbox</a>

                        <%-- NAVIGATION BAR --%>
                        <div class="navbar-content custyFonts">
                            <ul class="nav ">
                                <li>
                                    <%-- HOME PAGE --%>
                                    <a href="Default.aspx">Home</a>
                                </li>
                                <li>
                                    <%-- ABOUT US --%>
                                    <a href="#">About</a>
                                </li>
                                <li class="active">
                                    <%-- EVENT PREDICTOR --%>
                                    <a href="Event_Predictor.aspx">Predictor</a>
                                </li>
                                <li>
                                    <%-- EVENT SUBCRIBER --%>
                                    <a href="#">Subscriber</a>
                                </li>
                                <li>
                                    <%-- COMMANDER HELPER --%>
                                    <a href="#">Helper</a>
                                </li>
                                <li>
                                    <%-- EVENT WATCHER --%>
                                    <a href="Event_Watcher.aspx">Watcher</a>
                                </li>
                            </ul>
                        </div>
                        <%-- /NAVIGATION BAR --%>
                    </div>
                </div>
            </div>
            <div class="container">

                <%-- MAIN HEADER SECTION --%>
                <div class="hero-unit hidden-phone custyFontsBold">

                    <%-- Added custom font on 8/14/2013 (Brandon) --%>
                    <h1>Meta Event Predictor</h1>
                    <p>
                        <%-- CHOOSE YOUR REGION --%>
                        Choose Your Region:
                        <asp:DropDownList ID="RegionChoiceDDL" runat="server" AutoPostBack="true">
                            <asp:ListItem Value="1">North America</asp:ListItem>
                            <asp:ListItem Value="2">Europe</asp:ListItem>
                        </asp:DropDownList>

                        <%-- CHOOSE YOUR SERVER --%>
                        Choose Your Server:
                        <asp:DropDownList ID="ServerChoiceDDL" runat="server" AutoPostBack="true"></asp:DropDownList>
                    </p>
                    <p></p>
                </div>

                <%-- EVENT LISTINGS --%>
                <div class="row main-features">

                    <%-- NOTES
                         7-10-2013: Removed the LBL's and replaced the slots with tables.  This way, I will be
                                    able to create tables dynamically and reference them by name later on if
                                    needing to organize.

                         8-1-2013:  Removed LBL 10 from the listing, because it was not needed.  I should really
                                    end up making this a dynamic table driven application that I can
                                    change on the fly without actually updating hard-coded stuff.

                         8-14-2013: Added hard-set width and heights for the individual panels within the page.
                                    That way, there won't be any random moving around of the panels when they are
                                    within the maximum page width like they were doing before.

                    --%>

                    <%-- SLOT ONE --%>
                    <asp:Panel runat="server" CssClass="span4" ID="Panel1" Width="300" Height="150">
                        <asp:Panel runat="server" CssClass="well">
                            <asp:Table runat="server" ID="TimeSlotTBL_1"></asp:Table>
                        </asp:Panel>
                    </asp:Panel>

                    <%-- SLOT TWO --%>
                    <asp:Panel runat="server" CssClass="span4" ID="Panel2" Width="300" Height="150">
                        <asp:Panel runat="server" CssClass="well">
                            <asp:Table runat="server" ID="TimeSlotTBL_2"></asp:Table>
                        </asp:Panel>
                    </asp:Panel>

                    <%-- SLOT THREE --%>
                    <asp:Panel runat="server" CssClass="span4" ID="Panel3" Width="300" Height="150">
                        <asp:Panel runat="server" CssClass="well">
                            <asp:Table runat="server" ID="TimeSlotTBL_3"></asp:Table>
                        </asp:Panel>
                    </asp:Panel>

                    <%-- SLOT FOUR --%>
                    <asp:Panel runat="server" CssClass="span4" ID="Panel4" Width="300" Height="150">
                        <asp:Panel runat="server" CssClass="well">
                            <asp:Table runat="server" ID="TimeSlotTBL_4"></asp:Table>
                        </asp:Panel>
                    </asp:Panel>

                    <%-- SLOT FIVE --%>
                    <asp:Panel runat="server" CssClass="span4" ID="Panel5" Width="300" Height="150">
                        <asp:Panel runat="server" CssClass="well">
                            <asp:Table runat="server" ID="TimeSlotTBL_5"></asp:Table>
                        </asp:Panel>
                    </asp:Panel>

                    <%-- SLOT SIX --%>
                    <asp:Panel runat="server" CssClass="span4" ID="Panel6" Width="300" Height="150">
                        <asp:Panel runat="server" CssClass="well">
                            <asp:Table runat="server" ID="TimeSlotTBL_6"></asp:Table>
                        </asp:Panel>
                    </asp:Panel>

                    <%-- SLOT SEVEN --%>
                    <asp:Panel runat="server" CssClass="span4" ID="Panel7" Width="300" Height="150">
                        <asp:Panel runat="server" CssClass="well">
                            <asp:Table runat="server" ID="TimeSlotTBL_7"></asp:Table>
                        </asp:Panel>
                    </asp:Panel>

                    <%-- SLOT EIGHT --%>
                    <asp:Panel runat="server" CssClass="span4" ID="Panel8" Width="300" Height="150">
                        <asp:Panel runat="server" CssClass="well">
                            <asp:Table runat="server" ID="TimeSlotTBL_8"></asp:Table>
                        </asp:Panel>
                    </asp:Panel>

                    <%-- SLOT NINE --%>
                    <asp:Panel runat="server" CssClass="span4" ID="Panel9" Width="300" Height="150">
                        <asp:Panel runat="server" CssClass="well">
                            <asp:Table runat="server" ID="TimeSlotTBL_9"></asp:Table>
                        </asp:Panel>
                    </asp:Panel>

                </div>
                <%-- /EVENT LISTINGS --%>
            </div>
        </div>
    </form>
</body>

</html>
